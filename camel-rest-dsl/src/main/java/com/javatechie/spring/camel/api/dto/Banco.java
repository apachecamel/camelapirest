package com.javatechie.spring.camel.api.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Banco {
	
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private int categoria;
	private double saldo;
	
//	public Banco(Long id, String name, int categoria, double saldo) {
//		super();
//		this.id = id;
//		this.name = name;
//		this.categoria = categoria;
//		this.saldo = saldo;
//	}
	
	public double getSaldo() {
		return saldo;
	}


	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCategoria() {
		return categoria;
	}

	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}

}
