package com.javatechie.spring.camel.api.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.javatechie.spring.camel.api.dto.Banco;
import com.javatechie.spring.camel.api.service.BancoService;

@Component
public class BancoProcessor implements Processor{
	
	@Autowired
	private BancoService service;

	@Override
	public void process(Exchange exchange) throws Exception {
		service.addBanco(exchange.getIn().getBody(Banco.class));
	}

}
