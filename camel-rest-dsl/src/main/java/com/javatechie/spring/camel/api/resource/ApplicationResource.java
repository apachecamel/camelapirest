package com.javatechie.spring.camel.api.resource;

import org.apache.camel.BeanInject;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.javatechie.spring.camel.api.dto.Banco;
import com.javatechie.spring.camel.api.dto.BancoDao;
import com.javatechie.spring.camel.api.processor.BancoProcessor;
import com.javatechie.spring.camel.api.processor.ProcesadorBDD;
import com.javatechie.spring.camel.api.service.BancoService;

@Component
public class ApplicationResource extends RouteBuilder {

	@Autowired
	private BancoService service;
	
	@Autowired
	private BancoDao bancodao;

	@BeanInject
	private BancoProcessor processor;
	
	@BeanInject
	private ProcesadorBDD processorBDD;
	
	@Override
	public void configure() throws Exception {
		restConfiguration().component("servlet").port(9090).host("localhost").bindingMode(RestBindingMode.json);

		rest().get("/holamundo").produces(MediaType.APPLICATION_JSON_VALUE).route()
				.setBody(constant("Test basico de API GET")).endRest();

		rest().get("/getBanco").produces(MediaType.APPLICATION_JSON_VALUE).route().setBody(() -> service.getBanco())
				.endRest();

		rest().post("/addBanco").consumes(MediaType.APPLICATION_JSON_VALUE).type(Banco.class).outType(Banco.class)
				.route().process(processor).endRest();
		
		rest().post("/addBanco2").consumes(MediaType.APPLICATION_JSON_VALUE).type(Banco.class).outType(Banco.class)
				.route().process(processorBDD).endRest();
		
		rest().get("/getBanco2").produces(MediaType.APPLICATION_JSON_VALUE).route()
				.setBody(() -> bancodao.findAll()).endRest();
		
	}

}
