package com.javatechie.spring.camel.api.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.javatechie.spring.camel.api.dto.Banco;

@Service
public class BancoService {

	private List<Banco> list = new ArrayList<>();

	@PostConstruct
	public void initDB() {
	}

	public Banco addBanco(Banco banco) {
		list.add(banco);
		return banco;
	}

	public List<Banco> getBanco() {
		return list;
	}

}
